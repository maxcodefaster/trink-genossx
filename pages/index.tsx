import type {NextPage} from 'next'
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Canvas from "../components/canvas";
import React from "react";

const Home: NextPage = () => {
  let [brushColor, setBrushColor] = React.useState('#fa6988');

  return (
    <div className={styles.container}>
      <Head>
        <title>Trink-GenossX</title>
        <meta name="description" content="Trink-GenossX Logo Maker"/>
        <link rel="icon" href="/favicon.ico"/>
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Trink-Genoss<a href="https://trink-genosse.de" style={{color: brushColor}}>X</a>
        </h1>

        <div className={styles.grid}>
          <Canvas onColorChange={(color: string) => setBrushColor(color)}/>
        </div>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://heichling.xyz"
          target="_blank"
          rel="noopener noreferrer"
          style={{color: brushColor}}
        >
          Powered by heichling.xyz
        </a>
      </footer>
    </div>
  )
}

export default Home
