import CanvasDraw from "react-canvas-draw"
import styles from '../styles/canvas.module.css'
import {CompactPicker} from 'react-color';
import React from "react";

let savableCanvas: any = null;

function Canvas(props: any) {
  let [brushColor, setBrushColor] = React.useState('#fa6988');
  let [brushSize, setBrushSize] = React.useState(3);
  let [logoText, setLogoText] = React.useState('TRINK—\nGENOSSE\nCOOP—\nIDENTITY');
  let [loading, setLoading] = React.useState(false);

  function handleChangeComplete(color: { hex: string; }) {
    setBrushColor(color.hex);
    props.onColorChange(color.hex);
  }

  return (
    <div>
      <div style={{display: "flex", justifyContent: "space-evenly"}}>
        <textarea className={styles.inputText} maxLength={40}
                  value={logoText} onChange={(e) => setLogoText(e.target.value)}>
        </textarea>
        <button
          onClick={() => {
            if (logoText.length >= 40) return;
            setLogoText(logoText + '—')
          }} style={{color: 'black', height: '50%', alignSelf: 'center'}}
          className={styles.actionButton}>
          <i>Gimme &quot;—&quot;</i>
          <br/>
          <sub>Geviertstrich!</sub>
        </button>
      </div>
      <div className={styles.wrapper}>
        <CanvasDraw ref={canvasDraw => (savableCanvas = canvasDraw)} className={styles.canvas} canvasWidth={350}
                    canvasHeight={350} brushRadius={brushSize} hideGrid={true} brushColor={brushColor} lazyRadius={0}/>
        <img src="/trink-genosse.png" alt="trink genosse logo" className={styles.canvasLogo}/>
        <p className={styles.canvasText}>{logoText}</p>
      </div>
      <div className={`${styles.justifyCenter} ${styles.marginTop}`}>
        <CompactPicker color={brushColor} onChangeComplete={handleChangeComplete}/>
      </div>
      <div className={styles.justifyCenter}>
        <span>Brush Size:&nbsp; &nbsp;</span>
        <input type="number" min="1" max="50" className={styles.input} value={brushSize}
               onChange={(e) => setBrushSize(Number(e.target.value))}>
        </input>
      </div>
      <div className={styles.actionButtons}>
        <button onClick={async () => {
          setLoading(true)
          await exportCanvas(logoText)
          setLoading(false)
        }} style={{marginRight: '20px', backgroundColor: brushColor}} className={styles.actionButton}>
          <b>Save logo</b>
        </button>
        <button
          onClick={() => {
            if (!savableCanvas) return;
            savableCanvas.clear();
          }} style={{marginRight: '20px', backgroundColor: 'grey'}} className={styles.actionButton}
        >
          <i>Clear</i>
        </button>
        <button
          onClick={() => {
            if (!savableCanvas) return;
            savableCanvas.undo();
          }} style={{backgroundColor: 'grey'}} className={styles.actionButton}
        >
          <i>Undo</i>
        </button>
      </div>
      <div className={styles.justifyCenter}>
        {loading && <span><b><i>LOADING</i></b></span>}
      </div>
    </div>
  );
}

async function exportCanvas(logoText: string) {
  if (!savableCanvas) return;
  const canvas = document.createElement('canvas');
  const ctx: CanvasRenderingContext2D = canvas.getContext('2d') as CanvasRenderingContext2D;
  ctx.canvas.width = 350;
  ctx.canvas.height = 350;

  const drawingImg = new Image();
  drawingImg.src = await savableCanvas.getDataURL();
  await new Promise(res => setTimeout(res, 750));
  const trinkGenosseImg = new Image();
  trinkGenosseImg.src = "/trink-genosse.png";
  await new Promise(res => setTimeout(res, 750));

  ctx.drawImage(drawingImg, 0, 0, 350, 350);
  ctx.drawImage(trinkGenosseImg, 50, 50, 250, 250);
  ctx.font = 'bold 40px Arial';

  const words: string[] = logoText.split('\n');
  const xPos = 125;
  for (let i = 0; i < words.length; i++) {
    const yPos = 110 + ((i + 1) * 48);
    let shift = 0;
    for (let j = 0; j < words[i].length; j++) {
      const character = words[i][j];
      ctx.fillText(character, xPos + shift, yPos);
      shift += ctx.measureText(character).width;
    }
  }

  const dataURL = canvas.toDataURL("image/png");
  await new Promise(res => setTimeout(res, 500));
  const link = document.createElement('a');
  link.href = dataURL;
  link.download = 'Trink-GenossX.png';
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
}

export default Canvas
